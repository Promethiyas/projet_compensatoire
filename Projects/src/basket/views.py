from datetime import datetime

import requests
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.contrib.auth.models import User,auth
from django.contrib import messages


@login_required(login_url='/accounts/login/')
def index(request):

    date = datetime.today()

    return render(request, "index.html", context={"date": date})


def teams(request):

    date = datetime.today()

    return render(request, "index.html", context={"date": date})


@login_required(login_url='/accounts/login/')
def players_list(request):
    page = request.GET.get('page', 1)
    response = requests.get(f"https://www.balldontlie.io/api/v1/players?page={page}")
    players_data = response.json()
    return render(request, 'index.html', {'players_data': players_data})


@login_required(login_url='/accounts/login/')
def teams_list(request):
    page = request.GET.get('page', 1)
    response = requests.get(f"https://www.balldontlie.io/api/v1/teams?page={page}")
    teams_data = response.json()
    return render(request, 'teams.html', {'teams_data': teams_data})


@login_required(login_url='/accounts/login/')
def games_list(request):
    page = request.GET.get('page', 1)
    response = requests.get(f"https://www.balldontlie.io/api/v1/games?page={page}")
    games_data = response.json()
    return render(request, 'matchs.html', {'games_data': games_data})


@login_required(login_url='/accounts/login/')
def team_details(request, team_id):
    response = requests.get(f"https://www.balldontlie.io/api/v1/teams/{team_id}")
    team_data = response.json()
    players_response = requests.get(f"https://www.balldontlie.io/api/v1/players?team_ids[]={team_id}")
    players_data = players_response.json()
    return render(request, 'team_details.html', {'team_data': team_data, 'players_data': players_data})


@login_required(login_url='/accounts/login/')
def matchs_details(request, matchs_id):
    response = requests.get(f"https://www.balldontlie.io/api/v1/games/{matchs_id}")
    games_data = response.json()
    return render(request, 'matchs_details.html', {'games_data': games_data})




@login_required(login_url='/accounts/login/')
def players_infos(request, player_id):
    response = requests.get(f"https://www.balldontlie.io/api/v1/players/{player_id}")
    player_data = response.json()
    return render(request, 'players_infos.html', {'player_data': player_data})


def register(request):
    if request.method == 'POST':
        username1 = request.POST['username']
        email1 = request.POST['email']
        password = request.POST['password']
        password1 = request.POST['password1']
        if password == password1:
            if User.objects.filter(email=email1).exists():
                messages.info(request,'Email already registered')
                return redirect('register')
            elif User.objects.filter(username=username1).exists():
                messages.info(request,'This username is already taken')
                return redirect('register')
            else:
                user = User.objects.create_user(username=username1, email=email1, password=password)
                user.save();
                return redirect('/')
        else:
            messages.info(request,'Must use the same password')
            return redirect('register')
    else:
        return render(request, 'register.html')
