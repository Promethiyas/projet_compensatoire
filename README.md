Voici mon projet, pour l'utiliser vous devais avoir DJANGO d'installer. Après l'avoir git clone, utilisez GITBASH, rendez vous dans le projet à cet endroit 
```
projet_compensatoire/Projects/
```
puis entrez 
```
source .env/bin/activate
```
déplacez vous dans 
```
src/
```
et vous pouvez lancer le site avec
```
python manage.py runserver
```
Vous pouvez maintenant utiliser le site.
